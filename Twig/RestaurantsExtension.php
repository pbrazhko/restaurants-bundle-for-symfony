<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.02.15
 * Time: 19:40
 */

namespace CMS\RestaurantsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Environment;

class RestaurantsExtension extends \Twig_Extension
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('show_users_restaurants', array($this, 'showUsersRestaurants'), array('is_safe' => array('html'), 'needs_environment' => true))
        );
    }

    public function showUsersRestaurants(Twig_Environment $environment, $userId, $template = 'RestaurantsBundle:Twig:restaurants.html.twig')
    {
        $service = $this->container->get('cms.restaurants.service');

        $restaurants = array();
        $result = $service->findBy(array('create_by' => $userId, 'is_deleted' => false));

        if ($result) {
            $restaurants = $result;
        }

        return $environment->render($template, array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'cms_restaurants_extension';
    }
}