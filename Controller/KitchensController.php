<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 24.03.15
 * Time: 16:11
 */

namespace CMS\RestaurantsBundle\Controller;


use CMS\RestaurantsBundle\Entity\Kitchens;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class KitchensController
 * @package CMS\RestaurantsBundle\Controller
 */
class KitchensController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.restaurants.kitchens.service');

        return $this->render('RestaurantsBundle:Kitchens:list.html.twig', array(
            'kitchens' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.restaurants.kitchens.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_restaurants_kitchens_list'));
            }
        }

        return $this->render('RestaurantsBundle:Kitchens:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.restaurants.kitchens.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_restaurants_kitchens_list'));
            }
        }

        return $this->render('RestaurantsBundle:Kitchens:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.restaurants.kitchens.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_restaurants_kitchens_list'));
    }
}