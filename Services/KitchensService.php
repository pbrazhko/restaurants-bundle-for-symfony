<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 24.03.15
 * Time: 16:09
 */

namespace CMS\RestaurantsBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\RestaurantsBundle\Form\KitchensType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class KitchensService extends AbstractCoreService
{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new KitchensType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'RestaurantsBundle:Kitchens';
    }

}