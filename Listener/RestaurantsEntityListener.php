<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 07.02.16
 * Time: 15:26
 */

namespace CMS\RestaurantsBundle\Listener;


use CMS\RestaurantsBundle\Entity\Restaurants;
use CMS\RestaurantsBundle\Provider\ElasticaRestaurantsProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;

class RestaurantsEntityListener
{
    /**
     * @var ElasticaRestaurantsProvider
     */
    private $elasticaRestaurantsProvider;

    /**
     * RestaurantsEntityListener constructor.
     * @param ElasticaRestaurantsProvider $elasticaRestaurantsProvider
     */
    public function __construct(ElasticaRestaurantsProvider $elasticaRestaurantsProvider)
    {
        $this->elasticaRestaurantsProvider = $elasticaRestaurantsProvider;
    }


    public function postPersist(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Restaurants){
            return;
        }

        $this->elasticaRestaurantsProvider->addRestaurantToIndex($entity->getId());
    }

    public function postUpdate(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof Restaurants){
            return;
        }

        $this->elasticaRestaurantsProvider->updateRestaurantIndex($entity->getId());
    }
}