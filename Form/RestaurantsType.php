<?php

namespace CMS\RestaurantsBundle\Form;

use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\GeoBundle\Form\Types\GeometryType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RestaurantsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('city', LocaleEntityType::class, array(
                'class' => 'LocalizationBundle:Cities',
                'property' => 'title',
                'empty_data' => null,
                'placeholder' => '',
            ))
            ->add('address')
            ->add('busy_hours_start')
            ->add('busy_hours_end')
            ->add('days_off', ChoiceType::class, array(
                    'choices' => array(
                        'mon' => 'Monday',
                        'tue' => 'Tuesday',
                        'wed' => 'Wednesday',
                        'thu' => 'Thursday',
                        'fri' => 'Friday',
                        'sat' => 'Saturday',
                        'sun' => 'Sunday',
                    ),
                    'multiple' => true,
                    'expanded' => true
                )
            )
            ->add('kitchens', LocaleEntityType::class, array(
                'class' => 'RestaurantsBundle:Kitchens',
                'property' => 'title',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('descriptions', LocaleTextareaType::class, [
                'required' => false
            ])
            ->add('is_published')
            ->add('is_deleted')
            ->add('photos', ImagesType::class)
            ->add('location', GeometryType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\RestaurantsBundle\Entity\Restaurants',
            'translation_domain' => 'restaurants'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_restaurantsbundle_restaurants';
    }
}
