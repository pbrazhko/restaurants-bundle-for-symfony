<?php

namespace CMS\RestaurantsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RestaurantsBundle extends Bundle
{
    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return array(
            array(
                'title' => 'Restaurants',
                'defaultRoute' => 'cms_restaurants_list'
            ),
            array(
                'title' => 'Kitchens',
                'defaultRoute' => 'cms_restaurants_kitchens_list'
            )
        );
    }
}
