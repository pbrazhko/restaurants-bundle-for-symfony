<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.02.16
 * Time: 20:44
 */

namespace CMS\RestaurantsBundle\Normalizers;


use CMS\RestaurantsBundle\Entity\Restaurants;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class RestaurantsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Restaurants $object */
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'busy_hours_start' => $object->getBusyHoursStart(),
            'busy_hours_end' => $object->getBusyHoursEnd(),
            'days_off' => $object->getDaysOff(),
            'descriptions' => $object->getDescriptions(),
            'city' => $this->serializer->normalize($object->getCity(), $format, $context),
            'kitchens' => $this->serializer->normalize($object->getKitchens(), $format, $context),
            'photos' => $this->serializer->normalize($object->getPhotos(), $format, $context)
        ];
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Restaurants;
    }
}