<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 07.02.16
 * Time: 15:35
 */

namespace CMS\RestaurantsBundle\Provider;


use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\RestaurantsBundle\Entity\Kitchens;
use CMS\SearchBundle\Interfaces\SearchProviderInterface;
use CMS\SearchBundle\Services\SearchService;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Elastica\Query as EQ;

class SearchRestaurantsProvider implements SearchProviderInterface
{
    /**
     * @var TransformedFinder
     */
    private $restaurantsFinder;

    /**
     * SearchRestaurantsProvider constructor.
     * @param TransformedFinder $restaurantsFinder
     */
    public function __construct(TransformedFinder $restaurantsFinder)
    {
        $this->restaurantsFinder = $restaurantsFinder;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $defaultFieldsData
     * @return mixed
     */
    public function buildFilterForm(FormBuilderInterface $builder, array $defaultFieldsData = array())
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false,
                'translation_domain' => 'search'
            ])
            ->add('kitchens', LocaleEntityType::class, array(
                'class' => 'RestaurantsBundle:Kitchens',
                'property' => 'title',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'translation_domain' => 'search'
            ))
            ->add('city', LocaleEntityType::class, array(
                'class' => 'LocalizationBundle:Cities',
                'property' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ));
    }

    /**
     * @param Form $form
     * @param SearchService $searchService
     * @return mixed
     */
    public function search(Form $form, SearchService $searchService)
    {
        $boolQuery = new EQ\BoolQuery();

        if (null !== ($title = $this->getFormData($form, 'title'))) {

            $queryTerm = new EQ\Term();
            $queryTerm->setTerm('title.' . $searchService->getLocale(), mb_strtolower($title));

            $boolQuery->addMust($queryTerm);
        }

        if ($form->has('kitchens') && count($form->get('kitchens')->getData()) > 0) {

            $kitchensId = [];
            /** @var Kitchens $kitchen */
            foreach ($form->get('kitchens')->getData() as $kitchen) {
                $kitchensId[] = $kitchen->getId();
            }

            $queryTerms = new EQ\Terms();
            $queryTerms->setTerms('kitchens.id', $kitchensId);

            $boolQuery->addMust($queryTerms);
        }

        $sort = ['title.' . $searchService->getLocale() => ['order' => 'desc']];

        $query = new \Elastica\Query($boolQuery);
        $query->addSort($sort);

        $result = $this->restaurantsFinder->find($query, $searchService->getLimit());

        return $result;
    }

    private function getFormData(Form $form, $field, $default = null)
    {
        if ($form->has($field) && !$form->get($field)->isEmpty()) {
            return $form->get($field)->getData();
        }

        return $default;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'restaurants';
    }
}