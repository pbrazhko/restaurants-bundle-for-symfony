<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.02.16
 * Time: 20:22
 */

namespace CMS\RestaurantsBundle\Provider;


use CMS\GalleryBundle\Normalizer\ImagesNormalizer;
use CMS\LocalizationBundle\Normalizer\CitiesNormalizer;
use CMS\RestaurantsBundle\Normalizers\KitchensNormalizer;
use CMS\RestaurantsBundle\Normalizers\RestaurantsNormalizer;
use CMS\RestaurantsBundle\Services\RestaurantsService;
use Elastica\Document;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Type;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use FOS\ElasticaBundle\Provider\ProviderInterface;
use Monolog\Logger;
use Symfony\Component\Serializer\Serializer;

class ElasticaRestaurantsProvider implements ProviderInterface
{
    /**
     * @var Type
     */
    private $restaurantsType;

    /**
     * @var TransformedFinder
     */
    private $restaurantsFinder;

    /**
     * @var RestaurantsService
     */
    private $restaurantsService;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ElasticaRestaurantsProvider constructor.
     * @param Type $restaurantsType
     * @param TransformedFinder $restaurantsFinder
     * @param RestaurantsService $restaurantsService
     * @param Logger $logger
     */
    public function __construct(Type $restaurantsType, TransformedFinder $restaurantsFinder, RestaurantsService $restaurantsService, Logger $logger)
    {
        $this->restaurantsType = $restaurantsType;
        $this->restaurantsFinder = $restaurantsFinder;
        $this->restaurantsService = $restaurantsService;
        $this->logger = $logger;

        $normalizers = [
            new RestaurantsNormalizer(),
            new CitiesNormalizer(),
            new KitchensNormalizer(),
            new ImagesNormalizer()
        ];

        $this->serializer = new Serializer($normalizers, []);
    }

    public function findById($restaurantId){
        $query = new BoolQuery();

        $fieldQuery = new Match();
        $fieldQuery->setField('id', $restaurantId);

        $query->addShould($fieldQuery);

        return $this->restaurantsFinder->findHybrid($query);
    }

    public function addRestaurantToIndex($restaurantId){

        $results = $this->findById($restaurantId);

        if(!empty($results)){
            return $this->updateRestaurantIndex($restaurantId);
        }

        if(null !== $restaurant = $this->restaurantsService->findOneBy(['id' => $restaurantId])){
            try {
                $this->restaurantsType->addDocument(
                    (new Document())->setData(
                        $this->serializer->normalize($restaurant)
                    )
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    public function updateRestaurantIndex($restaurantId){

        $results = $this->findById($restaurantId);

        if(empty($results)){
            return $this->addRestaurantToIndex($restaurantId);
        }

        /** @var HybridResult $result */
        foreach ($results as $result) {
            try {
                $this->restaurantsType->updateDocument(
                    (new Document($result->getResult()->getId()))->setData($this->serializer->normalize($result->getTransformed()))
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    /**
     * Persists all domain objects to ElasticSearch for this provider.
     *
     * @param \Closure $loggerClosure
     * @param array $options
     *
     */
    public function populate(\Closure $loggerClosure = null, array $options = array())
    {
        $restaurants = $this->restaurantsService->findBy(['is_published' => true, 'is_deleted' => false]);

        if($restaurants){
            $documents = [];

            foreach($restaurants as $restaurant){
                $document = new Document();

                $document->setData(
                    $this->serializer->normalize($restaurant, 'json')
                );

                $documents[] = $document;
            }

            $this->restaurantsType->addDocuments($documents);
        }
    }
}